﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfCommand
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            CommandBinding commandOpen = new CommandBinding(ApplicationCommands.Open);
            commandOpen.Executed += CommandOpen_Executed;
            this.CommandBindings.Add(commandOpen);
        }

        private void CommandOpen_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            MessageBox.Show("Open");
        }

        private void CommandNew_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            MessageBox.Show("New");
        }

        private void CommandSave_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            MessageBox.Show("Save");
        }

        private void CommandSave_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
        }

        private void cmdAdd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            edNote.Text += "*";
        }
    }

    public class MyCommands
    {
        public static RoutedCommand cmdAdd { get; private set; } = new("cmdAdd", typeof(MainWindow));
    }
}
